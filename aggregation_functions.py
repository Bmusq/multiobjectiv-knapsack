import numpy as np
import random
from input import read_instance, read_result
from pls1 import pls1
from itertools import chain, combinations

def powerset(iterable, strict=False):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    if strict:
    	return chain.from_iterable(combinations(s, r) for r in range(len(s)))
    else:
    	return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

def weighted_sum(w, x):
	return np.sum(w*np.array(x))

def OWA(w, x):
	## decroissant
	o = sorted(x)
	o = np.array(o)
	return np.sum(np.array(w)*o)

def choquet(w, x):
	sorted_indexs = np.argsort(x)[::-1] ## maximisation
	sorted_solution = x[sorted_indexs]
	sorted_gains = [sorted_solution[i-1] - sorted_solution[i] for i in range(1,len(sorted_solution))]
	sorted_gains = [sorted_solution[0]] + sorted_gains
	res = 0
	string = ""
	for i in range(len(x)):
		string += " + "+str(sorted_gains[i])+"*"+str(w[tuple(set(sorted_indexs[i:,]))])
		res += sorted_gains[i]*w[tuple(set(sorted_indexs[i:,]))]
	print(string)
	return res

def create_choquet_weights(nb_objectives):
	d = {(): 0}
	all_subsets = powerset(list(range(nb_objectives)))
	for subset in all_subsets:
		maxa = 0
		all_subsets_subsets = powerset(list(subset), True)
		for s in all_subsets_subsets:
			maxa = max(d[s], maxa)
		d[subset] = random.uniform(maxa, 1)
	d[list(d)[-1]] = 1 #### full set equals to 1
	assert len(d) == 2**nb_objectives
	return d


def max_agg_f(agg_f, weights, solutions):
	max_f = 0
	max_s = 0
	for s in range(len(solutions)):
		res_f = agg_f(weights, solutions[s])
		print(solutions[s],"resultat = ",res_f)
		if res_f > max_f:
			max_f = res_f 
			max_s = s
	return max_f, max_s, solutions[max_s]



#################################
#		TESTING FUNCTION		#
#################################

def exe_pls1(size, batch_num):
	#### data project : 
    # size = 200
    # batch_num = 999
    ressource_folder = "./Instances"
    filename_instance = ressource_folder+"/"+size+"_items/2KP"+size+"-TA-"+batch_num+".dat"
    filename_result = ressource_folder+"/"+size+"_items/2KP"+size+"-TA-"+batch_num+".eff"

    W, items = read_instance(filename_instance)
    # values = read_result(filename_result)
    #plot(soluce)
    non_dom_solutions = pls1(W, items)
    return non_dom_solutions

if __name__ == '__main__':

	### cas d'école
	non_dom_solutions = np.array([[12, 3], [10, 5], [7, 7]])

	## weighted sum
	print("\n **** weighted sum **** ")
	weights = np.array([1,1.1]) # bi-obj
	agg_f = weighted_sum
	solution_opti = max_agg_f(agg_f, weights, non_dom_solutions)
	print("solution opti = ", solution_opti[2], " avec un score de ", solution_opti[0])

	## OWA
	print("\n **** OWA **** ")
	weights = np.array([1,1.1]) # bi-obj
	agg_f = OWA
	solution_opti = max_agg_f(agg_f, weights, non_dom_solutions)
	print("solution opti = ", solution_opti[2], " avec un score de ", solution_opti[0])

	## choquet
	print("\n **** choquet **** ")
	weights = create_choquet_weights(len(non_dom_solutions[0])) # bi-obj
	print("weights = ", weights)
	agg_f = choquet
	solution_opti = max_agg_f(agg_f, weights, non_dom_solutions)
	print("solution opti = ", solution_opti[2], " avec un score de ", solution_opti[0])

	###### with real data #####
	print("\n **** REAL DATA **** ")
	size = 10
	batch_num = 999
	non_dom_solutions = exe_pls1(str(size), str(batch_num))
	weights_2 = np.array([1,1.1]) # bi-obj
	weights_6 = np.array([1,1.2,0.8,1.4,2.0,1.6]) # 6-obj

	## weighted sum
	print("\n **** weighted sum **** ")
	agg_f = weighted_sum
	solution_opti = max_agg_f(agg_f, weights_6, non_dom_solutions)
	print("solution opti = ", solution_opti[2], " avec un score de ", solution_opti[0])

	## OWA
	print("\n **** OWA **** ")
	agg_f = OWA
	solution_opti = max_agg_f(agg_f, weights_6, non_dom_solutions)
	print("solution opti = ", solution_opti[2], " avec un score de ", solution_opti[0])

	## choquet
	print("\n **** choquet **** ")
	weights = create_choquet_weights(len(non_dom_solutions[0])) # bi-obj
	print("weights = ", weights)
	agg_f = choquet
	solution_opti = max_agg_f(agg_f, weights_6, non_dom_solutions)
	print("solution opti = ", solution_opti[2], " avec un score de ", solution_opti[0])
