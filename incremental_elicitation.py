import gurobipy as gp
from gurobipy import GRB
import argparse
import _pickle as pickle
import json
import numpy as np
import time
import datetime

from Decideur import Decideur
from optimum import optWeightedSum, optOWA
from pls1 import pls1
from input import read_instance2
from aggregation_functions import weighted_sum, OWA


def prepare_parameters(size, batch_num, nb_items, nb_objectives):
    #### data project : 
    ressource_folder = "./Instances"
    filename_instance = ressource_folder+"/"+size+"_items/2KP"+size+"-TA-"+batch_num+".dat"

    W, items = read_instance2(filename_instance, nb_items, nb_objectives)
    return W, items

#*********************** Incremenal Elicitation *****************************
def rollout(W, w, u, non_dom_solutions, non_dom_sol_allocation, decideur):
    delta = 1e-3
    set_constraints = []
    MMR = float("inf")
    MMRs = []
    #****************************** Main Loop
    while(MMR > delta):
        #****** Initialisation
        MR = -float("inf")
        y_MR = 0
        MMR = float("inf")
        x_min = 0
        y_MMR = 0
        x_MMR = 0
        weights_sol_y = 0
        weights_sol_x = 0
        res = 0
        mr_list = []

        for x in range(len(non_dom_solutions)):
            MR = -float("inf")
            weights_sol_y = None
            y_MR = None
            for y in range(len(non_dom_solutions)):
                if x != y :

                    # resolve PMR witch is a linear problem
                    model = gp.Model('PMR')
                    model.setParam('OutputFlag', False) # verbose off

                    weight_list = []
                    # Variables of PL are weights of aggregation function
                    for i in range(len(u[0])): # nb objectives
                        weight_list.append(model.addVar(lb=0.0, ub=1.0, vtype=GRB.CONTINUOUS, name="w"+str(i)))

                    # Weights must sum to one
                    model.addConstr(sum(weight_list) == 1, name='weights_1')

                    for i in range(len(set_constraints)) :
                        fwx = decideur.agg_f(weight_list, set_constraints[i][0])
                        fwy = decideur.agg_f(weight_list, set_constraints[i][1])
                        model.addConstr(fwx >= fwy, name="c"+str(i))                            

                    model.setObjective(decideur.agg_f(weight_list, non_dom_solutions[y]) - decideur.agg_f(weight_list, non_dom_solutions[x]), GRB.MAXIMIZE)
                    model.write('lp/PMR.lp')
                    model.optimize()

                    res = model.ObjVal

                    # Compile MR
                    if res > MR :
                        # print("MR")
                        # print("x = "+str(x))
                        # print("y = "+str(y))
                        y_MR = y
                        MR = res
                        weights_sol_y = [v.x for v in weight_list]

            # Compile MR
            mr_list.append(MR)
            if MR < MMR and weights_sol_y != None:
                # print("MMR")
                # print("x = "+str(x))
                # print("y = "+str(y))
                x_MMR = x
                y_MMR = y_MR
                MMR = MR
                weights_sol_x = weights_sol_y

        #*********** Prints
        # print(mr_list)
        # print("solutions = ", non_dom_solutions)
        # print("opti x = ", x_MMR)
        # print("opti y = ", y_MMR)
        # print("opti x = ", non_dom_solutions[x_MMR])
        # print("opti y = ", non_dom_solutions[y_MMR])
        # print("opti weights = ", weights_sol_x)
        # print("fwx = ", decideur.agg_f(Decideur.hidden_weights, non_dom_solutions[x_MMR]))
        # print("MMR = ", MMR)
        # print("MMR_normalized = ", MMR_normalized)


        #*********** Query
        print("Queries:",len(MMRs)," | ","MMR:",MMR, end='\r')
        MMRs.append(MMR)
        if MMR > delta:
            best_sol, worst_sol = decideur.ask_query(non_dom_solutions[x_MMR], non_dom_solutions[y_MMR])
            set_constraints.append((best_sol, worst_sol))

    #####################################
    #       RESULTS
    #####################################
    if decideur.agg_f == weighted_sum:
        opti_fwx, opti_allocation, opti_x = optWeightedSum(decideur.hidden_weights, w, u)
    else:
        opti_fwx, opti_allocation, opti_x = optOWA(decideur.hidden_weights, w, u)

    exec_fwx = decideur.agg_f(decideur.hidden_weights, non_dom_solutions[x_MMR])
    exec_x = non_dom_solutions[x_MMR]
    exec_x_allocation = non_dom_sol_allocation[x_MMR]

    nqueries = decideur.nb_queries
    print()
    print("Poids prédits = ", weights_sol_x)
    print("Poids cachés =  ", decideur.hidden_weights)
    print("x (id = ", x_MMR,") = ", exec_x)
    print("x optimal théorique = ", opti_x)
    print("fwx optimal exec= ", exec_fwx)
    print("fwx optimal théorique = ", opti_fwx)
    ############################################################

    return opti_x, opti_fwx, opti_allocation, exec_x, exec_fwx, exec_x_allocation, nqueries, MMRs


#####################################
#       PARAMETERS
#####################################
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--func", help="Type of Agreg function: 0 WS, 1 OWA", default=1, type=int, required=False)
    parser.add_argument("--size", help="Number of agents of instance", default="200", type=str, required=False)
    parser.add_argument("--batch", help="Number of the batch", default="999", type=str, required=False)
    parser.add_argument("--nitems", help="Number of items from the batch", default=20, type=int, required=False)
    parser.add_argument("--nobj", help="Number of objectives", default=4, type=int, required=False)
    parser.add_argument("--nexec", help="Number of simulations", default=1, type=int, required=False)

    algo_string = "PLS_EI"
    args = parser.parse_args()
    decideur = Decideur(args.nobj, args.func) # OWA: 1   |   SW: 0
    W, items = prepare_parameters(args.size, args.batch, args.nitems, args.nobj)

    MMR_queries = np.array(args.nexec)
    fun_string = ""
    if args.func == 0:
        fun_string = "WS"
        print("[Aggregation function]: Weighted Sum")
    else:
        fun_string = "OWA"
        print("[Aggregation function]: OWA")

    #************** Data Structure
    data_basile = {
        "opti_x": 0,
        "opti_fwx": 0,
        "opti_x_allocation": 0,
        "exec_x": 0,
        "exec_fwx": 0,
        "exec_x_allocation": 0,
        "Number of Questions": 0,
        "MMR": 0,
        "Duration (s)": 0
    }

    executions_times = np.zeros((args.nexec))
    data = {
        "opti_x": [],
        "opti_fwx": [],
        "opti_x_allocation": [],
        "exec_x": [],
        "exec_fwx": [],
        "exec_x_allocation": [],
        "nqueries": [],
        "mmrs": []
    }

    #************** Execution
    filename_basile = 'log/'+algo_string+"-"+fun_string+"-"+str(args.nexec)+"_"+datetime.datetime.now().strftime("%d-%b-%H-%M-%S")+'.json'
    for i in range(args.nexec):
        decideur.redrawWeights()
        decideur.resetNbQueries()

        print("------Execution", i)
        start_time = time.time()

        non_dom_solutions, non_dom_sol_allocation = pls1(W, items)
        items_array = np.array(items)
        w = items_array[:,0]
        u = items_array[:,1:]
        samples = rollout(W, w, u, non_dom_solutions, non_dom_sol_allocation, decideur)

        time_exec = time.time() - start_time 
        print("------time exec = ", time_exec)
        print()

        #***** Data Basile
        # Formatted data to log
        samples_basile = list(samples).copy()
        samples_basile[2] = sorted([i for i in range(len(samples_basile[2])) if samples_basile[2][i]==1])
        samples_basile[1] = round(samples_basile[1],3)
        samples_basile[3] = samples_basile[3].tolist()
        samples_basile[4] = round(samples_basile[4],3)
        samples_basile[5] = sorted(samples_basile[5])


        samples_basile.append(time_exec)
        for k,key in zip(range(len(samples_basile)),data_basile.keys()):
            data_basile[key] = samples_basile[k]

        with open(filename_basile, 'a') as fp:
            json.dump(data_basile, fp)
            fp.write("\n")

        #***** Data Marius
        for k,key in zip(range(len(samples)),data.keys()):
            data[key].append(samples[k])
        executions_times[i] = time_exec


    np.savetxt('log/'+algo_string+"-"+fun_string+"-"+str(args.nexec)+"_Times_"+datetime.datetime.now().strftime("%d-%b_%H-%M-%S")+'.txt',executions_times)
    filename = 'log/'+algo_string+"-"+fun_string+"-"+str(args.nexec)+"_Data_"+datetime.datetime.now().strftime("%d-%b_%H-%M-%S")+'.txt'
    with open(filename, 'wb') as f:
        pickle.dump(data, f)