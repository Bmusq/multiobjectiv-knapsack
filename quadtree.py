import numpy as np 



class Node():
    def __init__(self, objectives, successorship , children):
        self.objectives = objectives
        self.successorship  = successorship
        self.children = {}
        # self.root = root

    def calculate_successorship(self, child):
        bin_w = "".join([str(int(a)) for a in self.objectives > child.objectives])
        w = int(bin_w, 2)
        return w

    def rec_is_dom(self, x):
        '''
        returns 0 if x dominated
        returns 1 if x isn't dominated 
        '''
        succ_x = self.calculate_successorship(x)
        print("bin x successorship : ", bin(succ_x))
        if not '0' in bin(succ_x)[2:]: # x is dominated by self
            print("x is dominated by self")
            return 0

        for child in self.children:
            if self.children[child].successorship | succ_x == succ_x : # all x's zeros are in child
                if self.children[child].rec_is_dom(x) == 0: # x is dominated by a child
                        return 0
         
        # x isn't dominated by any node            
        return 1


    def rec_dom(self, x):
        '''
        returns 0 if self is dominated
        returns 1 if self isn't dominated 
        '''
        succ_x = self.calculate_successorship(x)
        print("bin x successorship : ", bin(succ_x))

        for child in self.children:
            if child & succ_x == succ_x : # all x's zeros are in child
                res = self.children[child].rec_is_dom(x)
                if res == 0: # x dominates child and it has no children
                    print("x dominates child, pop child")
                    self.children.pop(child) # we remove the dominated child
                    # del children[child] # ?
                elif res == 1: # x doesn't dominates child
                    print(" no dom ")
                    pass
                else : # x dominates child and it has children, we have to replace it with a new node
                    print("replace old child")
                    print(self.children[child])
                    self.children.pop(child) # pop the old child
                    self.children[child] = res # replace it by the new one
                    print(self.children[child])

        if succ_x == 0: # x dominates self
            print("x dominates self")
            if len(self.children) == 0: # no child, the parent node will have to delete this node
                print("no child")
                return 0
            else : # this node do have children, we pick one to replace him
                print("with child, return new succ")
                new_succ = self.children[self.children.keys[0]] # we pick a random children to replace him
                
                #### TO DO
                new_succ.children.append(self.children) # the chosen one will take self's children as his
                ####

                return children[new_succ] # the parent node can now delete self and add the new_succ
        return 1


    def dom_root(self, x):
        res = self.rec_dom(x)
        if res == 0:
            print("x dominates self with no child")
            return None # returning only x
        elif res == 1 :
            print("x doesn't dominate self")
            return self
        else :
            print("x dominates self with children")
            return res


    def insert_x(self, x):
        succ_x = self.calculate_successorship(x)
        print("bin x successorship : ", bin(succ_x))
        if not succ_x in self.children:
            self.children[succ_x] = x
        else:
            children[succ_x].insert_x(x)


    def insert(self, x):
        # calculating successorship
        succ = self.calculate_successorship(x)

        # 1. checking if some solution dominates x
        # solution that could dominate x are on the branchs with successorship that have all 1's from x's successorship
        if self.rec_is_dom(x) == 0:
            print("x is dominated")
            return self
        else :
            print("x isn't dominated")

        # 2. checking if some solutions are dominated by x
        # solution that could be dominated by x are on the branchs with successorship that have all 0's from x's successorship
        tree = self.dom_root(x)

        # 3. inserting x in tree
        if tree == None:
            return x
        else :
            self.insert_x(x)


    def print_rec(self):
        print(self.objectives)
        print(self.successorship)
        for c in self.children:
            print("children from ", self)
            self.children[c].print_rec()
            


if __name__ == '__main__':
    n = Node(np.array([10, 10, 10]), 0, [])
    c1 = Node(np.array([5, 5, 5]), 0, [])
    c2 = Node(np.array([5, 5, 12]), 0, [])
    c3 = Node(np.array([15, 15, 12]), 0, [])
    s1 = n.calculate_successorship(c1)
    s2 = n.calculate_successorship(c2)
    s3 = n.calculate_successorship(c3)
    # print(n.rec_is_dom(c1))
    # print(n.rec_is_dom(c2))
    # print(n.rec_is_dom(c3))
    # print(n.rec_dom(c1))
    # print(n.rec_dom(c2))
    # print(n.rec_dom(c3))
    n.insert(c1)
    n.insert(c2)
    n.insert(c3)
    # n.insert(c1)
    # n.print_rec()
    # n.insert(c2)
    # n.print_rec()
    # n.insert(c3)
    # print("foaeubfaoue")
    # n.print_rec()