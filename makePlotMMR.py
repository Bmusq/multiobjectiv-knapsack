import _pickle as pickle
import numpy as np
import math
import time
import datetime
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

if __name__ == '__main__':
	# parser = argparse.ArgumentParser()
	filename = "log/PLS_EI-WS-20_Data_22-Jan_16-49-41.txt"

	with open(filename, 'rb') as f:
		data = pickle.load(f)

		l_opti_x = data['opti_x']
		l_exec_x = data['exec_x']
		l_nqueries = data['nqueries']
		l_MMRs = data['mmrs']

		fn = filename.split("/")[1].split("-")
		print(fn)
		algo_string = fn[0]
		fun_string = fn[1]

		############################################
		# PRINT : pourcentage erreur
		############################################
		nb_erreurs = 0
		for i in range(len(l_opti_x)):
			if (l_opti_x[i] != l_exec_x[i]).any():
				nb_erreurs += 1
		print("Pourcentage d'erreur = ", nb_erreurs*100/len(l_opti_x), "%")

		print("Moyenne du nombre de questions = ", sum(l_nqueries)/len(l_nqueries))

		max_nb_queries = max(l_nqueries)
		max_nb_MMRs = max_nb_queries+1
		x = range(max_nb_MMRs)
		y_list = []
		for i in range(max_nb_MMRs):
			y_list.append([])
		
		nb_run_with_nb_queries = np.zeros(max_nb_MMRs)
		# Pour chaque run on additione le MMR de la j-ième itértion à ceux des autres runs (pour faire la moyenne)
		for i in range(len(l_MMRs)): # la i-ième run
			# Complete with 0
			mmrsToAdd = l_MMRs[i] + [0]*(max_nb_MMRs-len(l_MMRs[i]))
			print(len(mmrsToAdd))
			print(max_nb_MMRs)
			print(mmrsToAdd)
			for j in range(len(mmrsToAdd)): # la i-ième itération (j-ième question)
				y_list[j].append(mmrsToAdd[j])
			for j in range(len(l_MMRs[i])):
				nb_run_with_nb_queries[j] += 1
			
		# sum -> mean
		median = np.zeros(max_nb_MMRs)
		mean = np.zeros(max_nb_MMRs)
		for i in range(len(y_list)):
			median[i] = y_list[i][math.floor(len(y_list[i])/2)]
			mean[i] = sum(y_list[i])/len(y_list[i])

		fig,ax = plt.subplots()
		lns1 = ax.plot(x, mean, label='Mean MMR', color="blue")
		# ax.plot(x, median, label='Median MMR', color="green")
		ax.set_xlabel('nb queries asked')
		ax.set_ylabel('MMR')
		ax2=ax.twinx()
		lns2 = ax2.plot(x, nb_run_with_nb_queries, label='nb runs with k queries', color="red")
		ax2.set_ylabel("nb runs")
		ax2.yaxis.set_major_locator(MaxNLocator(integer=True))
		plt.title('Weighted Sum PLS + EI')
		lns = lns1+lns2
		labs = [l.get_label() for l in lns]
		ax.legend(lns, labs, loc=0)
		plt.show()
		fig.savefig("plots/"+algo_string+"_"+fun_string+"_MMR_"+str(datetime.datetime.now().strftime("%d_%b-%H_%M_%S"))+".jpg",
					format='jpeg',
					bbox_inches='tight')