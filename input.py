# Auteurs: Basile Musquer, Marius Le Chapelier
# Sorbonne Université
# MADMC Projet

import math
import numpy as np
import random

def getParameters(size, batch_num, nb_items, nb_objectives):
    ressource_folder = "./Instances"
    filename_instance = ressource_folder+"/"+size+"_items/2KP"+size+"-TA-"+batch_num+".dat"
    filename_result = ressource_folder+"/"+size+"_items/2KP"+size+"-TA-"+batch_num+".eff"

    W, items = read_instance2(filename_instance, nb_items, nb_objectives)
    return W, items

def read_instance(filename):
    """
        Read complete file
    """
    if (filename[-3:] != "dat"):
        print("Error file's extension must be .dat")
        exit(1)
    
    with open(filename) as datFile:
        lines = datFile.readlines()
        i = 0
        while(i<len(lines)-1 and lines[i][0] != "n"):
            i += 1
        nagent = int(lines[i].split()[1])
        print("Number of agents: ", nagent)
        i = len(lines)-1
        while(i>0 and lines[i][0] != "W"):
            i -= 1
        W = int(lines[i].split()[1])
        print("Bag' size: ", W)

        objects_list = []
        for line in lines:
            if line[0] == "i":
                split_line = line.split()[1:]
                objects_list.append([ int(split_line[i]) for i in range(len(split_line))]) #multiobj
        #print("Objects:", objects_list)
    return W, objects_list

def read_instance2(filename, nb_items, nb_objectives):
    """
        Return a given number of items for the given n-th first objectives
    """
    if (filename[-3:] != "dat"):
        print("Error file's extension must be .dat")
        exit(1)
    
    with open(filename) as datFile:
        lines = datFile.readlines()
        i = 0
        ### nagent
        while(i<len(lines)-1 and lines[i][0] != "n"):
            i += 1
        nagent = int(lines[i].split()[1])

        objects_list = []
        while(i<len(lines)-1 and lines[i][0] != "i"):
            i += 1
        j=0
        while(j<nb_items and i<len(lines)-1):
            line = lines[i]
            if line[0] == "i":
                split_line = line.split()[1:]
                objects_list.append([ int(split_line[i]) for i in range(min(nb_objectives+1,len(split_line)))]) #multiobj
            i += 1
            j+=1
        W = math.ceil(sum(np.array(objects_list)[:,0])/2)
        #print("Objects:", objects_list)
    return W, objects_list

def read_result(filename):
    '''
        Read a results from an .eff file
    '''
    print("RESULT")
    with open(filename) as fp:
        textfile = fp.read()
    textfile = textfile[0:len(textfile)-1]
    non_dom_points = [[],[]]
    lines = textfile.split("\n")
    for line in lines:
        v = line.split("\t")
        non_dom_points[0].append(int(v[0]))
        non_dom_points[1].append(int(v[1]))
    return non_dom_points