import matplotlib.pyplot as plt
import matplotlib as mpl
import pylab
import numpy as np
import datetime
import json

from function import addList

def graphHisto(logs, dtype):
    data = []
    for log,i in zip(logs,range(len(logs))): # 4 Procédures, 20 executions data.dim=(4,20)
        data.append([json.loads(line) for line in open(log, 'r')])

    names = []
    for log in logs:
        names.append("-".join(log[4:].split("-", 2)[:2]))

    x = np.arange(1, len(data)+1)
    plot_pt = np.zeros((len(data)))
    std = np.zeros((len(data)))

    for i in range(len(data)): # Taille: 4
        tmp=[]
        for j in range(len(data[i])): # Taille 20
            plot_pt[i] += data[i][j].get(dtype)
            tmp.append(data[i][j].get(dtype))
        plot_pt[i] /= len(data[i])
        std[i] = np.std(tmp)

    plt.rc("text", usetex=True)
    plt.rc("font", family="serif")
    mpl.style.use("seaborn-muted")

    fig, (ax1) = plt.subplots(
        1, sharey=False, sharex=False, figsize=(6.5, 5)
    )
    fig.subplots_adjust(bottom=0.3, left=0.10, right=0.98)

    ax1.grid()
    ax1.set_ylabel(dtype, fontsize=15)
    ax1.set_xlabel("Procedures", fontsize=15)
    ax1.text(
        0.5,
        -0.45,
        "Mean " + dtype + " of " + str(len(data[i])) + " Executions",
        horizontalalignment="center",
        transform=ax1.transAxes,
        fontsize=15,
    )

    plt.bar(x, plot_pt, 0.3, color='darkblue' )
    #plt.errorbar(x, plot_pt, yerr=std,fmt = 'none', capsize = 5, ecolor = 'red', elinewidth = 1.5, capthick = 2)
    plt.xlim(0,len(x)+1)
    pylab.xticks(x, names, rotation=40) # ajouter des labels aux bâtons
    plt.savefig("plot/"+dtype+"_"+datetime.datetime.now().strftime("%d-%b_%H-%M-%S")+".png")
    # plt.show()

def graphMMR(logs):
    data = []
    for log,i in zip(logs,range(len(logs))): # 4 Procédures, 20 executions data.dim=(4,20)
        data.append([json.loads(line) for line in open(log, 'r')])

    names = []
    for log in logs:
        names.append("-".join(log[4:].split("-", 2)[:2]))

    plt.rc("text", usetex=True)
    plt.rc("font", family="serif")
    mpl.style.use("seaborn-muted")

    fig, (ax) = plt.subplots(
        1, sharey=False, sharex=False, figsize=(6.5, 5)
    )
    fig.subplots_adjust(bottom=0.3, left=0.10, right=0.98)

    ax.grid()
    ax.set_ylabel("MMR", fontsize=15)
    ax.set_xlabel("Number of Queries", fontsize=15)
    ax.text(
        0.5,
        -0.30,
        "MMR's variation wrt Number of Queries for " + str(len(data[i])) + " Executions",
        horizontalalignment="center",
        transform=ax.transAxes,
        fontsize=15,
    )

    MMRs = [[] for i in range(len(data))]
    for i in range(len(data)): # Taille: 4
        for j in range(len(data[i])): # Taille 20
            MMRs[i] = addList(MMRs[i], data[i][j].get("MMR"))
        MMRs[i] = [MMRs[i][j]/len(data[i]) for j in range(len(MMRs[i]))]

    x = np.arange(0,max([len(MMRs[i]) for i in range(len(MMRs))]))
    max_queries = max([len(MMRs[i]) for i in range(len(MMRs))])
    for i in range(len(MMRs)):
        if len(MMRs[i]) < max_queries:
            MMRs[i] += [0]*(max_queries - len(MMRs[i]))
        ax.plot(x,MMRs[i], label='MMR '+names[i])

    ax.legend()
    plt.savefig("plot/MMR"+"_"+datetime.datetime.now().strftime("%d-%b_%H-%M-%S")+".png")
    # plt.show()

#--------------------------------------

if __name__ == '__main__':
    logs=[
        "log/RBLS-OWA-20_22-Jan-16-31-34.json",
        "log/RBLS-WS-20_22-Jan-16-31-37.json",
        "log/PLS_EI-OWA-20_22-Jan-17-57-28.json",
        "log/PLS_EI-WS-20_22-Jan-17-57-15.json"
    ]

    graphHisto(logs, dtype="Duration (s)")
    graphHisto(logs, dtype="Number of Questions")
    graphMMR(logs)
